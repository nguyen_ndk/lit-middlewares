const formidable = require('formidable');
const form = formidable({ multiples: true });

module.exports = async (req, res, next) => {
  form.parse(req, (e, fields, files) => {
    if (e) {
      next(e);
      return;
    }
    req.body = fields;
    req.files = files;
    next();
  });
};
