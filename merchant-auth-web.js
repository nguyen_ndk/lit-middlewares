const createError = require('http-errors')

const {AUTH, errorTags} = require('lit-constants')
const {facade: {EncryptFacade, Log}} = require('lit-utils')
const {StaffRepo} = require('lit-repositories')

const extractToken = req => {
  if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
    return req.headers.authorization.split(' ')[1]
  } else if (req.query && req.query.token) {
    return req.query.token
  }
  return null
}

const getStaff = async (accessKey) => {
  let tokenBody = {}
  try {
    tokenBody = EncryptFacade.decrypt(accessKey, AUTH.MERCHANT.SECRET.WEB)
  } catch (e) {
    Log.error('AUTH_OG_USER ERROR', e)
    throw createError(401, errorTags.UNAUTHORIZED)
  }
  return await StaffRepo.findById(tokenBody.id)
}
module.exports = async (req, res, next) => {
  const token = extractToken(req)
  if (!token) return next(createError(401, errorTags.UNAUTHORIZED))
  try {
    const staff = await getStaff(token)
    req.staff = staff
    next()
  } catch (e) {
    next(e)
  }
}