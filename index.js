module.exports = {
  jwt: require('./jwt'),
  merchantAuth: require('./merchant-auth'),
  litUserAuth: require('./lit-user-auth'),
  requestValidation: require('./request-validation'),
  merchantAuthWeb: require('./merchant-auth-web'),
  merchantStaffAuth: require('./merchant-staff-auth'),
  posAuth: require('./pos-auth'),
  mobileStaffAuth: require('./mobile-staff-auth'),
  mobileAppAuth: require('./mobile-app-auth'),
  appFlierAuth: require('./app-flier-auth'),
  formData: require('./form-data'),
};
