const httpError = require('http-errors');

const { AUTH, errorTags, POS_DEVICE, MERCHANT } = require('lit-constants');
const {
  facade: { TokenFacade },
} = require('lit-utils');
const { PosDeviceRepo, MerchantRepo } = require('lit-repositories');

const extractToken = (req) => {
  if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
    return req.headers.authorization.split(' ')[1];
  } else if (req.query && req.query.token) {
    return req.query.token;
  }
  return null;
};
const _decode = (token) => {
  try {
    return TokenFacade.verify(token, AUTH.MERCHANT.SECRET.POS);
  } catch (e) {
    throw httpError(401, errorTags.INVALID_TOKEN);
  }
};
module.exports = async (req, res, next) => {
  const token = extractToken(req);
  if (!token) return next(httpError(401, errorTags.UNAUTHORIZED));
  try {
    req.jwtDecoded = _decode(token);
    req.pos = await PosDeviceRepo.findById(req.jwtDecoded.posDeviceId);
    if (!req.pos || req.pos.status !== POS_DEVICE.STATUS.ACTIVE)
      return next(httpError(401, errorTags.POS_INACTIVE_OR_DELETED));
    req.merchant = await MerchantRepo.findById(req.jwtDecoded.merchantId);
    if (!req.merchant || req.merchant.status !== MERCHANT.STATUS.ACTIVE)
      return next(httpError(401, errorTags.MERCHANT_INACTIVE));
    next();
  } catch (e) {
    next(e);
  }
};
