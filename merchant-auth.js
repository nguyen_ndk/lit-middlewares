const _ = require('lodash');
const httpError = require('http-errors');

const { AUTH, errorTags } = require('lit-constants');
const {
  facade: { EncryptFacade, Log },
} = require('lit-utils');
const { MerchantRepo } = require('lit-repositories');

const extractAccessKey = (req) => {
  return _.get(req, 'headers.authorization', null);
};

const getMerchant = async (accessKey) => {
  let id = {};
  try {
    id = EncryptFacade.decrypt(accessKey, AUTH.MERCHANT.SECRET.API);
  } catch (e) {
    Log.error('AUTH_OG_USER ERROR', e);
    throw httpError(401, errorTags.UNAUTHORIZED);
  }
  return await MerchantRepo.findById(id);
};
module.exports = async (req, res, next) => {
  const accessKey = extractAccessKey(req);
  if (!accessKey) return next(httpError(401, errorTags.UNAUTHORIZED));
  try {
    const merchant = await getMerchant(accessKey);
    if (!merchant) return next(httpError(401, errorTags.UNAUTHORIZED));
    req.merchant = merchant;
    next();
  } catch (e) {
    next(e);
  }
};
