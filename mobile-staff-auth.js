const httpError = require('http-errors');

const { AUTH, errorTags, MERCHANT_STAFF, MERCHANT } = require('lit-constants');
const {
  facade: { TokenFacade },
} = require('lit-utils');
const { StaffRepo, MerchantRepo } = require('lit-repositories');

const extractToken = (req) => {
  if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
    return req.headers.authorization.split(' ')[1];
  } else if (req.query && req.query.token) {
    return req.query.token;
  }
  return null;
};
const _decode = (token) => {
  try {
    return TokenFacade.verify(token, AUTH.MERCHANT.SECRET.STAFF_MOBILE);
  } catch (e) {
    throw httpError(401, errorTags.INVALID_TOKEN);
  }
};
module.exports = async (req, res, next) => {
  const token = extractToken(req);
  if (!token) return next(httpError(401, errorTags.UNAUTHORIZED));
  try {
    req.jwtDecoded = _decode(token);
    req.staff = await StaffRepo.findById(req.jwtDecoded.staffId);
    if (!req.staff || req.staff.status !== MERCHANT_STAFF.STATUS.ACTIVE)
      return next(httpError(401, errorTags.STAFF_INACTIVE_OR_DELETED));
    req.merchant = await MerchantRepo.findById(req.jwtDecoded.merchantId);
    if (!req.merchant || req.merchant.status !== MERCHANT.STATUS.ACTIVE)
      return next(httpError(401, errorTags.MERCHANT_INACTIVE));
    next();
  } catch (e) {
    next(e);
  }
};
