const _ = require('lodash');
const httpError = require('http-errors');

const { AUTH_LIT_USER_TOKEN_SECRET, errorTags } = require('lit-constants');
const {
  facade: { TokenFacade, Log },
} = require('lit-utils');
const { LitUserRepo } = require('lit-repositories');

const extractToken = (req) => {
  if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
    return req.headers.authorization.split(' ')[1];
  } else if (req.query && req.query.token) {
    return req.query.token;
  }
  return null;
};

const getUsers = async (token) => {
  let tokenBody = {};
  try {
    tokenBody = TokenFacade.verify(token, AUTH_LIT_USER_TOKEN_SECRET);
  } catch (e) {
    Log.error('AUTH_OG_USER', e);
    throw httpError(401, errorTags.INVALID_TOKEN);
  }
  return await LitUserRepo.findByEmail(tokenBody.email);
};
module.exports = (roles) => async (req, res, next) => {
  if (_.isEmpty(roles)) return next();
  const token = extractToken(req);
  try {
    const ogUser = await getUsers(token);
    let userRoles = ogUser.role.split(',');
    let check;
    for (let i = 0; i < userRoles.length; i++) {
      if (roles.indexOf(userRoles[i]) != 1) {
        check = true;
      }
    }
    if (!check) {
      return next(httpError(403, errorTags.NOT_ALLOW));
    }

    req.ogUser = ogUser;
    next();
  } catch (e) {
    next(e);
  }
};
