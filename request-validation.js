const { validationResult } = require('express-validator')
const rTracer = require('cls-rtracer')
module.exports =  (req, res, next) => {
  const errors = validationResult(req)
  if (errors.isEmpty()) {
    return next()
  }
  const extractedErrors = []
  errors.array().map(err => extractedErrors.push({ [err.param]: err.msg }))

  return res.status(422).json({
    reqId: rTracer.id(),
    errors: extractedErrors,
  })
}